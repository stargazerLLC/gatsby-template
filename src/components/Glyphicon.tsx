import React from "react"
import colors from "@theme/colors"

export enum Icons {
  home = "\uE021",
  plane = "\uE039",
  ok = "\uE207",
  warning = "\uE197",
  okCircle = "\uE199",
  time = "\uE055",
  remove = "\uE208",
  trophy = "\uE075",
  cart = "\uE203",
  creditCard = "\uE271",
  back = "\uE225",
  forward = "\uE224",
}

interface GlyphiconProps {
  icon: keyof typeof Icons
  color?: keyof typeof colors
  size?: number
  margin?: string
  top?: number
  right?: number
  left?: number
  bottom?: number
  onClick?: () => void
}

const Glyphicon = ({
  icon,
  color,
  size,
  margin,
  top,
  right,
  left,
  bottom,
  onClick,
}: GlyphiconProps) => {
  return (
    <i
      onClick={onClick}
      style={{
        fontFamily: "glyphicons",
        fontStyle: "unset",
        color: colors[color || "primary"],
        fontSize: size,
        margin,
        top,
        right,
        left,
        bottom,
        position: top || right || left || bottom ? "absolute" : "unset",
      }}
    >
      {Icons[icon]}
    </i>
  )
}

Glyphicon.defaultProps = {
  size: 18,
}

export default Glyphicon
