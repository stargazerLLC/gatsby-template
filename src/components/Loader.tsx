import React from "react";
import { ClassNames } from "@emotion/core";

export default () => {
  const color = "#333";
  return (
    <>
      <ClassNames>
        {({ css, cx }) => (
          <div>
            <div
              className={cx(
                "spinner",
                css`
                  margin: 100px auto 0;
                  width: 95px;
                  text-align: center;

                  @-webkit-keyframes sk-bouncedelay {
                    0%,
                    80%,
                    100% {
                      -webkit-transform: scale(0);
                    }
                    40% {
                      -webkit-transform: scale(1);
                    }
                  }

                  @keyframes sk-bouncedelay {
                    0%,
                    80%,
                    100% {
                      -webkit-transform: scale(0);
                      transform: scale(0);
                    }
                    40% {
                      -webkit-transform: scale(1);
                      transform: scale(1);
                    }
                  }
                `
              )}
            >
              <div
                className={cx(
                  "bounce1",
                  css`
                    width: 18px;
                    height: 18px;
                    background-color: ${color};
                    margin: 5px;
                    border-radius: 100%;
                    display: inline-block;
                    -webkit-animation: sk-bouncedelay 1.4s infinite;
                    -webkit-animation-delay: -0.32s;
                    animation-delay: -0.32s;
                  `
                )}
              />
              <div
                className={cx(
                  "bounce2",
                  css`
                    width: 18px;
                    height: 18px;
                    background-color: ${color};
                    margin: 5px;
                    border-radius: 100%;
                    display: inline-block;
                    -webkit-animation: sk-bouncedelay 1.4s infinite;
                    -webkit-animation-delay: -0.16s;
                    animation-delay: -0.16s;
                  `
                )}
              />
              <div
                className={cx(
                  "bounce3",
                  css`
                    width: 18px;
                    height: 18px;
                    background-color: ${color};
                    margin: 5px;
                    border-radius: 100%;
                    display: inline-block;
                    -webkit-animation: sk-bouncedelay 1.4s infinite;
                  `
                )}
              />
            </div>
          </div>
        )}
      </ClassNames>
    </>
  );
};

