import React from "react"
import Glyphicon, { Icons } from "@components/Glyphicon"

const StyleGuide = (props: StyleGuideProps) => {
  return (
    <div>
      <h1>Stargazer Style Guide</h1>
      <p css={{ margin: "0 0 10px 5px" }}>
        These components are included in the default stargazer-gatsby package
      </p>

      <div>
        <h2 css={{ marginBottom: 10 }}>Glyphicons</h2>
        <div
          css={{
            display: "flex",
            flexWrap: "wrap",
            border: "3px solid black",
            margin: "0px 20px",
          }}
        >
          {Object.keys(Icons).map(key => (
            <div
              css={{
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
                margin: "3px",
                border: "1px solid #999",

                padding: "3px",
                minWidth: 75,
              }}
            >
              <Glyphicon size={25} icon={key} />
              {key}
            </div>
          ))}
        </div>

        <h1
          css={{
            marginTop: 25,
            fontWeight: "bold",
            textDecoration: "underline",
          }}
        >
          Text Guide
        </h1>
        <h1>Heading 1</h1>
        <h2>Heading 2</h2>
        <h3>Heading 3</h3>
        <h4>Heading 4</h4>
      </div>
    </div>
  )
}

export default StyleGuide
