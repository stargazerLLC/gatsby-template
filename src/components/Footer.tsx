import React from "react"
import colors from "@theme/colors"
import Glyphicon from "@components/Glyphicon"
const Footer = () => {
  return (
    <footer
      css={{
        backgroundColor: colors.primary,
        height: 40,
        color: colors.black,
        display: "flex",
        justifyContent: "space-between",
        alignItems: "flex-end",
      }}
    >
      <Glyphicon icon="plane" color="black" />
      <a
        href="https://www.gatsbyjs.org"
        css={{ ":visited": { color: colors.black } }}
      >
        Stargazer Web Design &amp; Marketing
      </a>
    </footer>
  )
}

export default Footer
