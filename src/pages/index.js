import React from "react"
import "normalize.css"
import Layout from "../components/layout"
import SEO from "../components/seo"
import StyleGuide from "@components/StyleGuide/StyleGuide"
const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <StyleGuide />
  </Layout>
)

export default IndexPage
