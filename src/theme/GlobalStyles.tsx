import React from "react"
import { Global, css } from "@emotion/core"
import colors from "./colors"

export default () => (
  <Global
    styles={css`
      @import url("https://fonts.googleapis.com/css?family=Cantarell|Roboto+Slab&display=swap");
      * {
        font-family: "Cantarell", sans-serif;
      }
      html {
        padding: 0;
        margin: 0;
        color: #000;
        background: ${colors.background};
      }
      body {
        padding: 0;
        margin: 0;
      }
      a {
        color: ${colors.black};
        text-decoration: none;
      }
      a:visited,
      a:active {
        color: ${colors.primary};
      }
      a:hover {
        color: ${colors.black};
      }

      h1,
      h2,
      h3,
      h4, p {
        margin: 0;
      }
    `}
  />
)
