enum colors {
  primary = "#458273",
  secondary = "#86251C",
  background = "#F5F5F5",
  red = "#AF240B",
  softWhite = "#f9f9f9",
  muted = "#841f0d",
  black = "#242628",
}

export default colors
